# Procès verbal du Conseil Exécutif de l'AGEEI du 18 avril 2019

## Ordre du Jour

1. Ouverture de la réunion
2. Adoption de l'ordre du jour
3. Adoption des minutes du 17 mars 2019
4. V.-p. interne\
 4.1. Dossier Bruno Malenfant\
 4.2. Évaluation de l'enseignement\
 4.3. Stages concernant les personnes vivant avec un handicap\
 4.4. État des installations d'informatique de l'UQÀM
5. V.-p. externe\
 5.1. Nomination de Fanny Lavergne comme adjointe
6. V.-p. aux technologies\
 6.1. Projet horaire-UQÀM\
 6.2. Nomination d'Alexandre Côté Cyr comme adjoint
7. V.-p. compétition\
 7.1. Bureau des compétitions\
 7.2. Site web\
 7.3. Accommodations pour le NorthSec
8. V.-p. loisir\
 8.1. Initiations 2019
9. Varia
10. Fermeture

## Ouverture de la réunion

Luis-Gaylor Nobre propose l'ouverture de la réunion.\
Philippe Grégoire appuie.\
La proposition est adoptée à l'unanimité.

## Adoption de l'ordre du jour

Luis-Gaylor Nobre propose l'adoption de l'ordre du jour.\
Philippe Grégoire appuie.\
La proposition est adoptée à l'unanimité.

## Adoption des minutes du 17 mars 2019

Armand propose l'adoption des minutes du 17 mars 2019.\
Philippe Grégoire appuie.\
La proposition est adoptée à l'unanimité.

## V.-p. interne

### Dossier Bruno Malenfant

Maxime Masson confirme avoir reçu de nombreux courriels et témoignages
d'élèves insatisfaits de l'enseignement de M. Malenfant. Ainsi, il portera
plainte à M. Villemaire ou M. Beaudry suite à la rédaction d'une plainte
officielle et la constitution d'un dossier.\
Armand propose de finir ce processus d'ici le 15 mai, ce qui est accepté
à l'unanimité.

### Évaluation de l'enseignement

Un membre de l'exécutif de l'AGEEI avait soulevé l'idée que l'association
pourrait prendre possession des évaluations de l'enseignement pour assurer
la mise en oeuvre des demandes des étudiants. Suite à une discussion avec
M. Villemaire, Charles Thérien rapporte que ce privilège ne sera pas
donner à l'association.

### Stages concernant les personnes vivant avec un handicap

Maxime Masson aimerait prendre en charge le dossier concernant un problème
technique que les élèves vivant avec un handicap ont lors de leur
recherche de stage. En effet, chaque étudiant doit faire le lien entre
l'intervenant et le bureau des stages. Maxime Masson aimerait donc
alléger cet inconvénient en rapprochant les intervenants au bureau des stages.

### État des installations d'informatique de l'UQÀM

Durant l’été, Philippe Grégoire propose de visiter les installations
du département d'informatique de différentes universités afin de noter
les ressources au support académique disponibles aux étudiants.
Aussi, le coût des cours, cotisations et nombre d’étudiants
seront recueillis afin d’établir un tableau comparatif des universités.
L’objectif est d’amener le département et l’université a augmenter
la quantité et la qualité des ressources mises à la dispositions des élèves.

## V.-p. externe

### Nomination de Fanny Lavergne comme adjointe

Dalila Boussetta propose Fanny Lavergne comme adjointe au poste de V.-p.
externe.\
Maxime Masson appuie.\
La proposition est adoptée à l'unanimité.

## V.-p. aux technologies

### Projet horaire-UQÀM

Philippe Grégoire présente l'ébauche de sa proposition au lien suivant:\
<https://gitlab.com/ageei/tech/horaires-uqam/blob/master/memo.md> \
Il aimerait un retour sur sa proposition d'ici le 15 mai.

### Nomination d'Alexandre Côté Cyr comme adjoint

Philippe Grégoire propose Alexandre Côté Cyr comme adjoint au poste de
V.-p. aux technologies.\
Luis-Gaylor Nobre appuie.\
La proposition est adoptée à l'unanimité.

## V.-p. compétition

### Bureau des compétitions

Philippe Van Velzen présente la charte du Bureau des compétitions.\
Il propose l'adoption de la charte du Bureau des compétitions.\
Luis-Gaylor appuie.\
La proposition est adoptée à l'unanimité.

### Site web

Le Bureau des compétitions aimerait que le site web prenne une direction
favorable aux commanditaires. Philippe Grégoire aimerait que le contenu
du site web soit la responsabilité des V.-p. correspondant. Luis-Gaylor
Nobre propose donc une réunion de l'exécutif pour que Philippe Grégoire
présente la structure du site web, pour faciliter la mise à jour du contenu.
Cette réunion aurait lieu mi-juin.

### Accommodations pour le NorthSec

Philippe Grégoire pense que ce serait pertinent de donner un allocation
alimentaire aux étudiants qui participent au NorthSec.\
Philippe Grégoire propose donc 800.00$ de fonds pour les divers repas.\
2 repas à 5$ et 3 repas à 12$ par repas sur présentation de facture, pour
tous les étudiants participants (16 personnes).\
Jean Philippe Robitaille-Larratt appuie.\
La proposition est adoptée à l'unanimité.

## V.-p. loisir

### Initiations 2019

Luis-Gaylor Norbre fera un post-mortem avec Marie-Pier Lessard des
initiations de 2018.\
Luis-Gaylor Nobre aimerait que les démarches commencent en juillet
et que tout l'exécutif s'implique. Il faudrait trouver des idées de
kiosque et il pourrait y avoir un sondage auprès des initiés
antérieurs pour connaitre leur opinion de ce qu'ils ont vécu.\
Armand Brière, le trésorier, aimerait que le budget soit établit avant
août.

## Varia

Charles Thérien explique que l'accès aux évaluations de l'enseignement ne
sera pas donné à l'exécutif puisque les informations pourraient être
diffusés publiquement.\
M. Villemaire a conseillé à Charles Thérien de consulter le conseil
académique pour réinstaurer l'évaluation des démonstrateurs.

## Fermeture

Philippe Grégoire propose la fermeture de la réunion.\
Luis-Gaylor Nobre appuie.\
La proposition est adoptée à l'unanimité.

## Présences

- Charles Thérien
- Philippe Grégoire
- Luis-Gaylor Nobre
- Philippe Van Velzen
- Armand Brière
- Dalila Boussetta
- Maxime Masson
- Alexis Bouchard
- Jean Philippe Robitaille-Larratt
