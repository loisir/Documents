# Assemblée générale du 3 octobre

## Ordre du jour 2018-10-03

1. Ouverture de l'assemblée
2. Adoption de l'ordre du jour
3. Adoption du pv du 20 septembre
4. Modification à la charte
   1. Officier de première année
   2. Adjoint
   3. VP Externe
5. Élections
   1. Trésorier
   2. VP Externe
   3. VP Interne
6. Vote sur le budget
7. Varia
8. Fermeture

## 1. Ouverture de l'assemblée

Francis Pelletier propose l'ouverture de l'assemblée.
Alexandre Côté Cyr appuie.
La proposition est adoptée à l'unanimité.

## 2. Adoption de l'ordre du jour

Samuel Castilloux propose l'adoption de l'ordre du jour.
Jean Salama appuie.
La proposition est adoptée à l'unanimité.

## 3. Adoption du pv du 20 septembre

Luis Nobre propose l'adoption du pv du 20 septembre 2018.
Phillipe Grégoire appuie.
La proposition est adoptée à l'unanimité.

## 4. Modification à la charte

### 4.1. Officier de première année

"Le poste d'officier de première année se veut une introduction au conseil
exécutif. Il se veut d'une implication plus flexible, mais surtout d'une
représentation au conseil de chaque nouvelle cohorte afin de favoriser le
contact et l'intégration de celle-ci au reste des étudiant.e.s de l'AGEEI.

Pour appliquer à ce poste, l'étudiant.e doit en être au plus à sa deuxième
session lors de la  session d'automne et conséquemment, peut seulement être
élu.e pendant cette session. Un maximum d'un seul mandat peut être rempli par
un.e même étudiant.e, à la fin duquel la personne est libre (et fortement
encouragée) d'appliquer à un autre poste du conseil exécutif.

Ce poste comporte les mêmes responsabilités et pouvoirs que les autres
officier.e.s de l'AGEEI."

Francis Pelletier propose d'ajouter ce poste à la charte.
Dalila Boussetta appuie

Phillipe Grégoire propose d'amender la proposition pour retirer le "d'un an" de
la phrase " Un maximum d'un seul mandat d'un an peut être rempli...
Dalila Boussetta appuie.
La proposition est adoptée à l'unanimité.

Francis Pelletier propose d'adopter la proposition amendée.
Dalila Boussetta appuie.
La proposition est adoptée à l'unanimité.

### 4.2. Adjoint

"Chacun des postes du comité exécutif, à l'exception de l'officier.e de première
année, peuvent se doter d'un.e adjoint.e. Les adjoints ne possèdent aucun
pouvoir ni responsabilité et ne sont pas des officier.e.s de l'AGEEI. Les
adjoint.e.s n'ont pas de droit de vote lors des conseils exécutifs."

Francis Pelletier propose d'ajouter ces postes à la charte de l'AGEEI.
David Thibaut Grenier appuie.
La proposition est adoptée à l'unanimité.

### 4.3. VP Externe

Description actuelle: "Le vice-président aux affaires externes est responsable
de la diffusion de l'information entourant les activités de l'Association
(différents sites web, réseaux sociaux, listes de diffusion, etc.). Il a pour
mandat de gérer l'image de l'Association ainsi que de créer des liens avec la
communauté (entreprises, groupes usagers, etc.)."

Nouvelle description proposée: "Le vice-président aux affaires externes s'assure
des relations externes de l'AGEEI, soit envers l'AESS, ses associations
modulaires, les autres associations et instances de l'UQAM et des autres
universités, ainsi que les entreprises et autres organismes, selon les besoins.
Il a pour mandat de gérer l'image de l'Association ainsi que de créer des liens
avec la communauté (entreprises, groupes usagers, etc.)."

Francis Pelletier propose de modifier la définition de la tâche du VP externe
pour qu'elle se lise comme ci-haut. Phillipe Grégoire appuie. La proposition est
adoptée à l'unanimité.

## 5. Élections

Alexandre Côté Cyr propose d'amender le point Élection pour y ajouter l'élection
pour officier de premiere année. Francis Pelletier appuie. La proposition est
adoptée à l'unanimité.

### 5.1. Trésorier

Armand Brière dépose sa candidature pour être trésorier.
Il est élu à majorité.

### 5.2. VP Externe

Dalila Boussetta dépose sa candidature pour être VP externe.
Elle est élue à majorité.

### 5.3. VP Interne

Rachel Michaud dépose sa candidature pour être VP interne.
Elle est élue à majorité.

### 5.4. Officier de première année

Alexis Bouchard dépose sa candidature pour être officier de première année.
Il est élu à majorité.

## 6. Vote sur le budget

Présentation du [budget prévisionnel](https://drive.google.com/open?id=1CJxL1mPQrvgtkSMr1C8AvpvFY-LZxJIrf68whQv838M).

Jean Salama propose le budget prévisionnel.
Philippe Van Velzen appuie.
La proposition est adoptée à l'unanimité.

## 7. Varia

## 8. Fermeture

David Thibaut Grenier propose la fermeture de l'assemblée.
Maxime Masson appuie.
La proposition est adoptée à l'unanimité.
