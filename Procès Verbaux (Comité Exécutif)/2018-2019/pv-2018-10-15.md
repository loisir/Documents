# Procès verbal du Conseil Exécutif de l'AGEEI du 15 octobre 2018

## Ordre du jour 15-10-2018

1. Ouverture
2. Adoption de l'ordre du jour
3. Adoption des minutes de l'AG du 3 octobre 2018
4. V.-p. aux affaires internes
    1. Demande d'équivalence
    2. Message courriel
5. V.-p. aux affaires externes
    1. Journée carrière
6. Trésorier
    1. IEEExtreme
7. V.-p. exécutif
8. Officier de première année
9. V.-p. aux compétitions
10. V.-p. aux loisirs
    1. Tag à l'arc
    2. Halloween
    3. V.-p. adjoint
11. V.-p. aux technologies
    1. Courriel pour officier de première année
    2. Site Web
    3. Adjoints
12. Varia
13. Fermeture

## Ouverture

Charles Thérien propose l'ouverture de la réunion.
Philippe Van Velzen appuie.
La proposition est adoptée à l'unanimité.

## Adoption de l'ordre du jour

Philippe Van Velzen propose l'adoption de l'ordre du jour.
Alexis Bouchard appuie.
La proposition est adoptée à l'unanimité.

## Adoption des minutes de l'AG du 3 octobre 2018

Alexis Bouchard propose l'adoption des minutes de l'AG du 3 octobre 2018.
Armand Brière appuie.
La proposition est adoptée à l'unanimité.

## V.-p. Interne

### Demande d'équivalence

Réception d'un courriel d'un.e étudiant.e insatisfait.e de la décision prise par
l'administration pour lui accorder (ou non) des crédit pour des cours faits
ailleurs.

Démarche proposé par l'association : prendre rendez-vous avec Roger Villemaire
pour discuter de son cheminement académique. Amener à cette rencontre
documentations et arguments.

### Message courriel

Petit rappel pour le CE de vérifier le formattage des emails qui sont envoyés à
tous avec le message de confirmation.

## V.-p. externe

### Journée carrière

Tous se déroule bien, 5 compagnies ont donné signe d'intérêt.

## Trésorier

### IEEExtreme

Confirmation de la démarche nécéssaire pour obtenir un remboursement: envoyer la
facture en pdf au trésorier et avoir sa présence à la compétition confirmée.

## V.-p. exécutif

Charles Thérien se propose comme v.-p. exécutif.
Rachel Bouchard appuie.
La proposition est adoptée à l'unanimité.

## Officier de première année

## V.-p. aux compétitions

IEEE: 75 participants attendus, dont 10 de l'UQAM.
Hackfest: 2 participants de l'UQAM confirmé, 1 en attente.

## V.-p. aux loisirs

### Tag à l'arc

10 inscriptions jusqu'à maintenant.

### Halloween

4 associations se regroupent au Fractal pour cette célébration (incluant
l'AGEEI). Amène l'idée d'utiliser le local commun pour faire un vestiaire.

### V.-p. adjoint

Maxime Masson est intéressé à se joindre à l'équipe. Il veut organiser des 5 à 7
(sous forme soit de présentation et réseautage ou de CodeNChill suivit de
bière).

Luis Nobre propose l'élection de Maxime Masson comme adjoint aux loisirs.
Philippe Van Velzen appuie.
La proposition est adoptée à l'unanimité.

## V.-p. aux technologies

### Courriel pour officier de première année

Dalila Boussetta propose "premiere_annee@ageei.org".
Alexis Bouchard appuie.
La proposition est adoptée à l'unanimité.

### Iite Web

### Adjoints

Discution sur nos options pour la gestion des identifiants des adjoints (email,
gDrive, #exec sur slack).

## Varia

## Fermeture

Charles Thérien propose la fermeture de la réunion.
Armand Brière appuie.
La proposition est adoptée à l'unanimité.
