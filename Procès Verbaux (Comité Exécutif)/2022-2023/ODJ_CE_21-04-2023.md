# Ordre du jour du CE le 21 avril 2023

## Présentation de l’ordre du jour

1. Lecture et adoption de l'ordre du jour
2. Présentation des adjoint.es
3. Formation harcèlement
4. Élection vice président.e exécutif
5. Party de fin de session
6. Comité loisirs
7. CTF ageei
8. Budget été
9. Calendrier d'événements 2023-2024
10. Discussions ambassadeur/drice.s
11. Clef du local
12. Le mail <executif@ageei.org>
13. Gilab – Approval
14. Bot discord AGEEI pour l'envoi d'annonces
15. Écriture inclusive
16. Sélection des participant.e.s Nsec
17. Nettoyage du frigo

## Ouverture

### Proposition 1 : Ouverture du CE

Proposition d'ouverture du CE.
Dûment proposée;
Dûment appuyée.
CE ouvert à 17:25.

## Lecture et adoption de l'ordre du jour

- L'ordre du jour est adoptée à l'unanimité.

## Présentation des adjoint.es

- François Beaumier, nouvel adjoint au VP Externe
- Alexandre Dumoulin, nouvel adjoint au VP Loisirs
- Marianne Boyer, nouvelle adjointe au VP Interne
- Stefan Voicu, nouvel adjoint au VP Techno

## Formation harcèlement

- **Formation sur la prévention des violences à caractères sexuels :**
  - Réserver la date pour les nouveaux membres
  - Voir si les anciens membres ont besoin de la refaire

## Élection vice président.e exécutif

- Carl-William Savaria-Bilodeau, VP loisirs, est élu vice-président à l'unanimité.

## Party de fin de session

- **Où** : La Petite Grenouille sur St-Laurent
- **Quand** : Vendredi le 5 mai

- **Senti** : 2-3 personnes sobres responsablent des étudiants pour la soirée
  - 51$ par communauto pour la soirée
  - Rémunération?

- **Billets** :
  - 10$/billet juste pour les membres de l’AGEEI
  - 1 billet donne accès à 5 consommations
  - Aucun shooter ou bouteille d’eau contre un ticket (aviser le bar)
  - Les non-membres payent leurs propres consommations
  - Les membres de l’exécutif ne payent pas leur billet
  - Les adjoints/adjointes et les membres du comité loisirs payent leur billet
  - Mettre sur les billets “Buvez avec parcimonie, soyez responsable!”

- **Bar** :
  - Vestiaire disponible et aucun coût d’entrée
  - Coûte 3000$ pour 75 personnes (5 consommations par personne)
  - 1000$ de budget pour des shooters (25$ pour 25 shooters)

- **Utilisation de Zeffy** :
  - Plateforme complètement gratuite (aucuns frais de transaction)
  - S'assurer que la communauté étudiante retire le pourboire de 18% pour ne pas payer des frais supplémentaires

### Proposition 2 : Émise par Armand, dûment appuyée par Alex

AUCUNS transports payés par l’AGEEI pour ce party.
Elliot propose le vote.
Adoptée à majorité.

## Comité loisirs

- **Approbation à l'unanimité des 8 nouveaux membres dans le comité loisirs :**
  - Carl-William Bilodeau-Savaria
  - Samuel Rochon
  - Elodie Brunet
  - Alexandre Dumoulin
  - Étienne Comptois
  - Lukas Poulin
  - Keven Jude Antenor
  - Derick Gagnon
- S'assurer que les membres du comité contactent au nom du comité loisirs (pas au nom de l’AGEEI)

### Proposition 3 : Émise par Guillaume, dûment appuyée par William

Maximum de 10 personnes au sein du comité.
Le président propose le vote.
Adoptée à l’unanimité.

## CTF ageei

### Proposition 4 : Émise par Armand, dûment appuyée par tous

Archivage des challenges CTFD et sauvegarde automatique des BD sur le gitlab de l’AGEEI.
Armand propose le vote.
Adoptée à l’unanimité.

## Budget été

- Faire une AG pour allouer du budget pour l’été (conférences, évènements, compétitions, etc.)
- **Possibilité budget :** (nb d’étudiants inscrits à la session d’été) x 18 = budget alloué
- **Mado le 24 mai :**
  - Évènement au Mado organisé par *Ellecode*
  - Possibilité de payer la moitié du coût des billets (~14$)

### Proposition 5 : Émise par Alex, dûment appuyée par Elliott

Le CE va organiser un AG la première partie du mois de mai et l'annoncer 14 jours à l'avance.
Alex propose le vote.
Adoptée à l’unanimité.

### Proposition 6 : Émise par Alex, dûment appuyée par Armand

Le comité exécutif s’engage à discuter d’un budget d’été qui ne dépasse pas 5000$.
Alex propose le vote.
Adoptée à l’unanimité.

## Calendrier d'événements 2023-2024

### Proposition 7 : Émise par Armand, dûment appuyée par Guillaume

Tous les VP donnent leurs dates d’évènements (au moins une session complète). Les évènements seront affichées dans un calendrier sur le site internet de l’AGEEI avant septembre. Exemples des évènements pris en compte : Party, activités, évènements, compétitions, midi-conférences, etc.
Armand propose le vote.
Adoptée à l’unanimité.

## Discussions ambassadeur/drice.s

- **Qui sont-ils?**
  - Des personnes diplomées qui promouvoient l’UQAM sur le marché du travail et ramènent du savoir à l’UQAM
  - Ces personnes diplomées sont engagées sur le durée de leur mandat dans la charte
  - On les enlève car ces personnes n’ont plus raison d’être

## Clef du local

- 1 clé à faire du local commun (Samuel)
- 3 clés à faire du local AGEEI (Samuel, Armand, Elodie)

## Le mail 'executif@ageei.org'

- Donner les accès à tous les membres du comité exécutif
- Le président et la secrétaire s’engagent à regarder les courriels souvent et s’occuper de les transférer aux gens concernés

## Gilab – Approval

### Proposition 8 : Émise par Elliott, dûment appuyée par Kim :**</ins>

Descendre le nombre de membres pour approuver les merges sur le gitlab.
Elliot propose le vote.
Adoptée à l'unanimité.

## Bot discord AGEEI pour l'envoi d'annonces

- On va en discuter au prochain CE

## Écriture inclusive

- L’exécutif s’engage à utiliser l’écriture épiscène (non genrée) dans les communications de l’AGEEI
- **Exemple :** la communauté étudiante

## Sélection des participant.e.s Nsec

- 8 billets disponibles (2 billets CTF-conférence, 6 billets CTF)
- Aucun traitement de faveur, pas de qualification sans faire les write-up
- Si une personne sélectionnée se désiste, une autre personne parmis ceux qualifiés sera sélectionnée
- Ceux qui envoient le 26 avril sont plus haut dans la sélection mais jusqu’au 1 mai non-officiellement pour envoyer

## Nettoyage du frigo

### Proposition 9 : Émise par Guillaume, dûment appuyée par Elliott

Laver le réfrigérateur chaque semaine, possibilité d’écrire nos noms sur nos repas.
Les restants de pizza commandés lors des évènements ne seront pas jetés.
Elliott propose le vote.
Adoptée à l’unanimité.

## Fermeture

### Proposition 10 : Fermeture du CE

Proposition de fermeture du CE.
Dûment proposée;
Dûment appuyée.
CE fermé à 19:54.
