# Procès verbal du Conseil Exécutif de l'AGEEI du 16 juin 2022

## Ordre du jour

0. Comment ça va?
1. Lecture et adoption de l'ordre du jour
2. Allocation du budget de 2000$ pour le local
3. Allocation du budget pour le(s) robot(s)
4. Retour sur la situation MAT4681
5. Discussion sur les compétitions pour l'été
6. Discussion activité code&chill
7. Discussion formation harcelement
8. Varia

## Ouverture

Lancelot propose l'ouverture, appuyé par Kim.

## Comment ça va

Discussion générale sur l'état de la vie de tout le monde.

## Lecture et adoption de l'ordre du jour

Présentation de l'ordre du jour par Lancelot.

Lancelot propose l'adoption de l'ordre du jour, c'est appuyé.
Adopté à l'unanimité.

## Allocation du budget de 2000$ pour le local

Proposition d'un montant de 250$ pour un autocollant pour la porte par Lancelot, appuyé par Kim.
Adopté à l'unanimité.

Proposition de 650$ pour 2 écrans et 100$ pour 2 claviers et souris par Carl-Eliott, appuyé par Alexandre.
Adopté à l'unanimité.

Proposition de 250$ pour l'éclairage par Lancelot, appuyé par Alexandre.
Adopté à l'unanimité.

Proposition de 750$ pour les chaises par Alexandre, appuyé par Lancelot.
Adopté à l'unanimité.

## Allocation du budget pour le(s) robot(s)

Remise à plus tard.

## Retour sur la situation MAT4681

Discussion sur la situation.
Reformuler le message.

## Discussion sur les compétitions pour l'été

Discussion générale.
Participation à MontreHack à chaque mois.

## Discussion activité code&chill

Aucun progrès.
Discussion générale.
Remis à plus tard.

## Discussion formation harcèlement

Discussion générale.
Discussions avec l'AESS, décalées à l'automne.

## Varia

- Ménage
- Imprimante 3D
- Conseil académique

## Fermeture

Lancelot propose la fermeture, appuyé par Kim.
Adopté à l'unanimité.
