# Procès verbal du Conseil Exécutif de l'AGEEI du 5 janvier 2022

## Ordre du jour

1. Ouverture
2. Lecture et adoption de l'ordre du jour
3. Retour A21
4. Petit Nobel?
5. Prochain party
6. Chalet?
7. Midi conférence H22
8. Tournoi d'échec ?
9. CTF H22
10. Site web
11. Refonte infra
12. Mercredi projection ?
13. Serveur Minecraft
14. Situation cours distance par rapport au évents
15. Nettoyage du local
16. Peinture
17. Varia
18. Fermeture

## Ouverture

Ouverture à 17h12.

## Adoption de l'ordre du jour

L'ordre du jour est adopté à l'unanimité.

## Retour A21

Bonne année, l'asso à réaliser beaucoup de projets et tout le monde à participer.

## Petit Nobel

Introduire un prix non-officiel pour féliciter les profs et les démos qui font
un bon travail. Voir comment on remet les «Prix» aux gagnants. Combiner les
profs et les chargés de cours. Pour la session d'hiver 2022.

## Prochain party/Chalet/événements Covid

La partie de hockey est toujours possible, Simon va s'en occuper en mi-février.
Party de mi-session risque de ne pas se produire, party de fin de session
reste à voir. Ont pourrait voir pour organiser des événements via zoom.
Zachary s'en occupe.

## Chalet

Chalet est cancellé.

## Midi conférence H22

Vlad, est intéréssé à présenté. Demandé à Privat d'envoyer un email
au prof pour qu'ils puissent présenter leurs recherches. Ont va
suivre les modalités d'enseignements.

## Tournoi d'échec

Ça c'était bien passer dernière fois. P-O peut s'en occuper,
on vas décider du format en fonction du nombre d'inscriptions.

## CTF H22

Ont pourrait essayer d'organiser quelque chose pour le NordSec, on
va voir le nombres d'équipe à envoyer en fonction de la taille des
équipes (ont peux envoyer entre 20-30 personnes).

## Site web

Bravo P-O le site web est vraiment joli.

## Refonte infra

Lancelot va remonter le serveur aux complet, puisque il n'est pas
récupérable dans sont état actuelle.

## Mercredi projection

Post-poned par le covid.

## Serveur Minecraft

On prend pas d'engagement avec Uqam Gaming, on le laisse sur Azur,
on va voir se qui ce passe avec dans 3 mois. Il faut réapliquer pour
la bourse en juin. Alex, Lancelot et Zach vont être mods (pour l'instant).

## Situation cours distance par rapport au évents

On suit les modalités d'enseignements pour les évenements. On instaure
le port de masque dans le local, en fonction du nombre de cas.

## Nettoyage du local

On doit aller au local, pour nettoyer le local. Fanny va faire
un suivi pour savoir l'accès.

## Peinture

Les peintures sont finies mais ont doit replacer le local.
Samedi le 15 janvier.

## Varia

## Fermeture

Simon propose la fermeture, Armand appuie.
